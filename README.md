# laravel build

Create a file of environment
```
cp .env.example .env
touch .php_cs.cache
```
start project
```
./project.sh up -d --build
```

./project.sh web composer update
./project.sh web php artisan migrate

http://localhost/

command for style fix
```
php-cs-fixer fix
```
